from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

from apps.users.api.views import UserViewSet
from apps.statistics.api.views import (CountryViewSet, SeasonViewSet,
                                       LeagueViewSet, TeamViewSet,
                                       PlayerViewSet, MatchViewSet,
                                       LeagueSeasonViewSet,
                                       MatchPlayerViewSet)
from apps.notify.api.views import (NotificationViewSet)
from apps.analyses.api.views import (MatchAnalyseViewSet,
                                     AnalyseViewSet,
                                     DataRowViewSet)

admin.autodiscover()

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'countries', CountryViewSet)
router.register(r'seasons', SeasonViewSet)
router.register(r'leagues', LeagueViewSet)
router.register(r'leagueseasons', LeagueSeasonViewSet)
router.register(r'teams', TeamViewSet)
router.register(r'players', PlayerViewSet)
router.register(r'matches', MatchViewSet)
router.register(r'match_players', MatchPlayerViewSet)
router.register(r'notifications', NotificationViewSet)
router.register(r'analyses', AnalyseViewSet)
router.register(r'data_rows', DataRowViewSet)
router.register(r'match_analyses', MatchAnalyseViewSet)

apipatterns = patterns('',
    url(r'auth/', include('apps.authentication.api.urls')),
    url(r'^api-token-auth/$', obtain_auth_token),
    url(r'^', include(router.urls))
)

urlpatterns = patterns('',
    url(r'^api/', include(apipatterns)),
    url(r'^admin/', include(admin.site.urls)),
)