import sys
sys.path.insert(0, '/home/adrian/dev/surebet-backend/src')

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings.dev'

import django
django.setup()

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class ClientFunctionalTest(object):
    TIMEOUT = 10

    def setup(self):
        self.driver = webdriver.Firefox()
        self.url = 'http://localhost/surebet-client/'
        self.driver.get(self.url)
        self.user = {
            "username": "test",
            "first_name": "test",
            "last_name": "test",
            "email": "test@test.com",
            "password": "test"
        }

    def test_registration(self):
        print "Testing registration page..."
        self.driver.get(self.url)
        registration_link = WebDriverWait(self.driver, self.TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, "/html/body/div/div/div/div/div/div/div[2]/form/fieldset/a"))
        )
        registration_link.click()
        submit_button = WebDriverWait(self.driver, self.TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, "/html/body/div/div/div/div/div/div/div[2]/form/fieldset/div[7]/button"))
        )

        username_input = self.driver.find_element_by_xpath('//*[@id="username"]')
        first_name_input = self.driver.find_element_by_xpath('//*[@id="first_name"]')
        last_name_input = self.driver.find_element_by_xpath('//*[@id="last_name"]')
        email_input = self.driver.find_element_by_xpath('//*[@id="email"]')
        password_input = self.driver.find_element_by_xpath('//*[@id="password"]')

        username_input.send_keys(self.user['username'])
        first_name_input.send_keys(self.user['first_name'])
        last_name_input.send_keys(self.user['last_name'])
        email_input.send_keys(self.user['email'])
        password_input.send_keys(self.user['password'])

        submit_button.click()

        success_toast = WebDriverWait(self.driver, self.TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="toast-container"]/div/div[2]/div'))
        )

        print "Testing registration page was successful"

    def test_login(self):
        print "Testing login page..."
        self.driver.get(self.url)
        submit_element = WebDriverWait(self.driver, self.TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH,
                                            "/html/body/div/div/div/div/div/div/div[2]/form/fieldset/div[3]/button"))
        )
        username_input = self.driver.find_element_by_xpath("/html/body/div/div/div/div/div/div/div[2]/form/fieldset/div[1]/input")
        password_input = self.driver.find_element_by_xpath("/html/body/div/div/div/div/div/div/div[2]/form/fieldset/div[2]/input")
        username_input.send_keys(self.user['username'])
        password_input.send_keys(self.user['password'])
        submit_element.click()
        header_element = WebDriverWait(self.driver, self.TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="wrapper"]/nav/div[1]/div/a'))
        )
        print "Testing login page was successful"

    def run_test(self):
        self.setup()
        try:
            self.test_registration()
            self.test_login()
        except:
            raise
        finally:
            self.tear_down()

    def tear_down(self):
        try:
            user = User.objects.get(username="test")
        except ObjectDoesNotExist:
            pass
        else:
            user.delete()
        if self.driver:
            self.driver.quit()

if __name__ == "__main__":
    ClientFunctionalTest().run_test()