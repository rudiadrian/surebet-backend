from locust import HttpLocust, TaskSet, task

class UserBehavior(TaskSet):
    def on_start(self):
        pass

    @task(1)
    def players(self):
        self.client.get('/api/players/')

    @task(1)
    def teams(self):
        self.client.get('/api/teams/')

    @task(3)
    def match_analyses(self):
        self.client.get('/api/match_analyses/')


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 4000
    max_wait = 9000