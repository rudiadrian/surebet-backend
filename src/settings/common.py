"""
Django settings for surebet project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PROJECT_ROOT = os.path.dirname(__file__)

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'a&_acou9=gsysx89s+v#*8g5#7&&cse+7a+%!^-yglqc^nrew5'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

sys.path.insert(0, os.path.join(PROJECT_ROOT, 'apps'))

INSTALLED_APPS = (
    ##### Default apps #####
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    ##### Third party apps #####
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    'django_filters',
    'notifications',
    'gunicorn'
)

PROJECT_APPS = (
    'surebet',
    'apps.authentication',
    'apps.statistics',
    'apps.notify',
    'apps.analyses'
)

INSTALLED_APPS += PROJECT_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'surebet.urls'

WSGI_APPLICATION = 'surebet.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pl'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_PATH = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    STATIC_PATH,
)

TEMPLATE_PATH = os.path.join(BASE_DIR, 'templates')

TEMPLATE_DIRS = (
    TEMPLATE_PATH,
)

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    )
}

# Django Cors Headers
# https://github.com/ottoyiu/django-cors-headers/

CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = (
    'localhost',
)

# Template Context Processors

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)

# Analyses
NEURAL_NETWORKS_STORAGE = os.path.join(PROJECT_PATH, 'neural_networks_storage')
GLOBAL_NEURAL_NETWORK_DATA_FILE = 'global_neural_network_data.csv'
GLOBAL_NEURAL_NETWORK_DATA_FILE_PATH = os.path.join(NEURAL_NETWORKS_STORAGE,
                                                    GLOBAL_NEURAL_NETWORK_DATA_FILE)
GLOBAL_NEURAL_NETWORK_FILE = 'global_neural_network.xml'
GLOBAL_NEURAL_NETWORK_FILE_PATH = os.path.join(NEURAL_NETWORKS_STORAGE,
                                               GLOBAL_NEURAL_NETWORK_FILE)

DECISION_TREES_STORAGE = os.path.join(PROJECT_PATH, 'decision_trees_storage')

GLOBAL_DECISION_TREE_FILE = 'global_decision_tree.pkl'
GLOBAL_DECISION_TREE_FILE_PATH = os.path.join(DECISION_TREES_STORAGE,
                                               GLOBAL_DECISION_TREE_FILE)

AUTH_PROFILE_MODULE = 'authentication.UserProfile'