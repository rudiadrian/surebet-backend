from django.db.models.aggregates import Avg, Sum
from django.db.models import Q, F
from rest_framework import serializers

from .. import models


class CountrySerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    players = serializers.HyperlinkedIdentityField(view_name='country-players',
                                                   read_only=True)
    teams = serializers.HyperlinkedIdentityField(view_name='country-teams',
                                                 read_only=True)

    class Meta:
        model = models.Country

class SeasonSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        if obj.start_year != obj.end_year:
            return '/'.join([str(obj.start_year), str(obj.end_year)])
        else:
            return str(obj.start_year)

    class Meta:
        model = models.Season


class LeagueSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    seasons = serializers.HyperlinkedIdentityField(view_name='league-seasons',
                                                   read_only=True)

    class Meta:
        model = models.League


class LeagueDetailSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    country = CountrySerializer(many=False)
    seasons = SeasonSerializer(many=True)

    class Meta:
        model = models.League

class CountryLeagueSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    seasons = SeasonSerializer(many=True)

    class Meta:
        model = models.League
        fields = ('url', 'id', 'name', 'slug', 'seasons')


class TeamSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    players = serializers.HyperlinkedIdentityField(view_name='team-players',
                                                   read_only=True)
    country = CountrySerializer()

    class Meta:
        model = models.Team


class TeamStandingsSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    won = serializers.SerializerMethodField()
    draw = serializers.SerializerMethodField()
    lost = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()
    played = serializers.SerializerMethodField()
    home_won = serializers.SerializerMethodField()
    home_draw = serializers.SerializerMethodField()
    home_lost = serializers.SerializerMethodField()
    home_points = serializers.SerializerMethodField()
    home_played = serializers.SerializerMethodField()
    away_won = serializers.SerializerMethodField()
    away_draw = serializers.SerializerMethodField()
    away_lost = serializers.SerializerMethodField()
    away_points = serializers.SerializerMethodField()
    away_played = serializers.SerializerMethodField()

    def get_won(self, obj):
        league_season_pk = self.context['league_season_pk']
        won = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(Q(home_team=obj) & Q(home_score__gt=F('away_score'))) |
                    Q(Q(away_team=obj) & Q(home_score__lt=F('away_score'))))\
            .count()
        return won

    def get_draw(self, obj):
        league_season_pk = self.context['league_season_pk']
        draw = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(Q(home_team=obj) & Q(home_score=F('away_score'))) |
                    Q(Q(away_team=obj) & Q(home_score=F('away_score'))))\
            .count()
        return draw

    def get_lost(self, obj):
        league_season_pk = self.context['league_season_pk']
        lost = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(Q(home_team=obj) & Q(home_score__lt=F('away_score'))) |
                    Q(Q(away_team=obj) & Q(home_score__gt=F('away_score'))))\
            .count()
        return lost

    def get_points(self, obj):
        return 3 * self.get_won(obj) + self.get_draw(obj)

    def get_played(self, obj):
        league_season_pk = self.context['league_season_pk']
        played = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(Q(home_team=obj) | Q(away_team=obj)))\
            .count()
        return played

    def get_home_won(self, obj):
        league_season_pk = self.context['league_season_pk']
        won = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(home_team=obj) & Q(home_score__gt=F('away_score')))\
            .count()
        return won

    def get_home_draw(self, obj):
        league_season_pk = self.context['league_season_pk']
        draw = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(home_team=obj) & Q(home_score=F('away_score')))\
            .count()
        return draw

    def get_home_lost(self, obj):
        league_season_pk = self.context['league_season_pk']
        lost = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(home_team=obj) & Q(home_score__lt=F('away_score')))\
            .count()
        return lost

    def get_home_points(self, obj):
        return 3 * self.get_home_won(obj) + self.get_home_draw(obj)

    def get_home_played(self, obj):
        league_season_pk = self.context['league_season_pk']
        played = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(home_team=obj)\
            .count()
        return played

    def get_away_won(self, obj):
        league_season_pk = self.context['league_season_pk']
        won = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(away_team=obj) & Q(home_score__lt=F('away_score')))\
            .count()
        return won

    def get_away_draw(self, obj):
        league_season_pk = self.context['league_season_pk']
        draw = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(away_team=obj) & Q(home_score=F('away_score')))\
            .count()
        return draw

    def get_away_lost(self, obj):
        league_season_pk = self.context['league_season_pk']
        lost = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(Q(away_team=obj) & Q(home_score__gt=F('away_score')))\
            .count()
        return lost

    def get_away_points(self, obj):
        return 3 * self.get_away_won(obj) + self.get_away_draw(obj)

    def get_away_played(self, obj):
        league_season_pk = self.context['league_season_pk']
        played = models.Match.historical\
            .filter(league_season__pk=league_season_pk)\
            .filter(away_team=obj)\
            .count()
        return played

    class Meta:
        model = models.Team


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    current_team = TeamSerializer()
    nationality = CountrySerializer()
    full_name = serializers.SerializerMethodField()

    def get_home_average_rate(self, obj):
        return models.HomeMatchPlayer.objects.filter(player=obj)\
            .aggregate(Avg('rating'))

    def get_away_average_rate(self, obj):
        return models.AwayMatchPlayer.objects.filter(player=obj)\
            .aggregate(Avg('rating'))

    def get_full_name(self, obj):
        return ' '.join([obj.first_name, obj.last_name])

    class Meta:
        model = models.Player


class TeamPlayerSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    nationality = CountrySerializer()
    full_name = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    goals = serializers.SerializerMethodField()
    appearances = serializers.SerializerMethodField()
    home_rating = serializers.SerializerMethodField()
    home_goals = serializers.SerializerMethodField()
    home_appearances = serializers.SerializerMethodField()
    away_rating = serializers.SerializerMethodField()
    away_goals = serializers.SerializerMethodField()
    away_appearances = serializers.SerializerMethodField()


    def get_full_name(self, obj):
        return ' '.join([obj.first_name, obj.last_name])

    def get_rating(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='home') & Q(match__home_team=obj.current_team)
                | Q(Q(side='away') & Q(match__away_team=obj.current_team))))\
            .aggregate(Avg('rating'))

    def get_goals(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='home') & Q(match__home_team=obj.current_team)
                | Q(Q(side='away') & Q(match__away_team=obj.current_team))))\
            .aggregate(Sum('goals'))

    def get_appearances(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='home') & Q(match__home_team=obj.current_team)
                | Q(Q(side='away') & Q(match__away_team=obj.current_team))))\
            .count()

    def get_home_rating(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='home') & Q(match__home_team=obj.current_team)))\
            .aggregate(Avg('rating'))

    def get_home_goals(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='home') & Q(match__home_team=obj.current_team)))\
            .aggregate(Sum('goals'))

    def get_home_appearances(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='home') & Q(match__home_team=obj.current_team)))\
            .count()

    def get_away_rating(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='away') & Q(match__away_team=obj.current_team)))\
            .aggregate(Avg('rating'))

    def get_away_goals(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='away') & Q(match__away_team=obj.current_team)))\
            .aggregate(Sum('goals'))

    def get_away_appearances(self, obj):
        return models.MatchPlayer.objects.filter(player=obj)\
            .filter(Q(Q(side='away') & Q(match__away_team=obj.current_team)))\
            .count()

    class Meta:
        model = models.Player


class LeagueSeasonSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    league = LeagueSerializer()
    season = SeasonSerializer()

    class Meta:
        model = models.LeagueSeason


class LeagueSeasonPlayerSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    league = LeagueSerializer()
    season = SeasonSerializer()
    played = serializers.SerializerMethodField()
    goals = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    home_played = serializers.SerializerMethodField()
    home_goals = serializers.SerializerMethodField()
    home_rating = serializers.SerializerMethodField()
    away_played = serializers.SerializerMethodField()
    away_goals = serializers.SerializerMethodField()
    away_rating = serializers.SerializerMethodField()

    def get_played(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        played = models.MatchPlayer.objects\
            .filter(player=player, match__league_season=obj)\
            .filter(Q(match__home_team=player.current_team) | Q(match__away_team=player.current_team))\
            .count()
        return played

    def get_goals(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        goals = models.MatchPlayer.objects\
            .filter(player=player,match__league_season=obj)\
            .filter(Q(match__home_team=player.current_team) | Q(match__away_team=player.current_team))\
            .aggregate(Sum('goals'))
        return goals

    def get_rating(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        rating = models.MatchPlayer.objects\
            .filter(player=player, match__league_season=obj)\
            .filter(Q(match__home_team=player.current_team) | Q(match__away_team=player.current_team))\
            .aggregate(Avg('rating'))
        return rating


    def get_home_played(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        played = models.MatchPlayer.objects\
            .filter(player=player, match__league_season=obj)\
            .filter(match__home_team=player.current_team)\
            .count()
        return played

    def get_home_goals(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        goals = models.MatchPlayer.objects\
            .filter(player=player,match__league_season=obj)\
            .filter(match__home_team=player.current_team)\
            .aggregate(Sum('goals'))
        return goals

    def get_home_rating(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        rating = models.MatchPlayer.objects\
            .filter(player=player, match__league_season=obj)\
            .filter(match__home_team=player.current_team)\
            .aggregate(Avg('rating'))
        return rating

    def get_away_played(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        played = models.MatchPlayer.objects\
            .filter(player=player, match__league_season=obj)\
            .filter(match__away_team=player.current_team)\
            .count()
        return played

    def get_away_goals(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        goals = models.MatchPlayer.objects\
            .filter(player=player,match__league_season=obj)\
            .filter(match__away_team=player.current_team)\
            .aggregate(Sum('goals'))
        return goals

    def get_away_rating(self, obj):
        player_pk = self.context['player_pk']
        player = models.Player.objects.get(pk=player_pk)
        rating = models.MatchPlayer.objects\
            .filter(player=player, match__league_season=obj)\
            .filter(match__away_team=player.current_team)\
            .aggregate(Avg('rating'))
        return rating

    class Meta:
        model = models.LeagueSeason


class LeagueSeasonTeamSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    league = LeagueSerializer()
    season = SeasonSerializer()
    played = serializers.SerializerMethodField()
    won = serializers.SerializerMethodField()
    draw = serializers.SerializerMethodField()
    lost = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()
    home_played = serializers.SerializerMethodField()
    home_won = serializers.SerializerMethodField()
    home_draw = serializers.SerializerMethodField()
    home_lost = serializers.SerializerMethodField()
    home_points = serializers.SerializerMethodField()
    away_played = serializers.SerializerMethodField()
    away_won = serializers.SerializerMethodField()
    away_draw = serializers.SerializerMethodField()
    away_lost = serializers.SerializerMethodField()
    away_points = serializers.SerializerMethodField()

    def get_played(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        played = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(home_team=team) | Q(away_team=team))\
            .count()
        return played

    def get_won(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(Q(home_team=team) & Q(home_score__gt=F('away_score'))) |
                    Q(Q(away_team=team) & Q(away_score__gt=F('home_score'))))\
        .count()
        return won

    def get_draw(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        draw = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(Q(home_team=team) | Q(away_team=team)) &
                    Q(home_score=F('away_score')))\
            .count()
        return draw

    def get_lost(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(Q(home_team=team) & Q(home_score__lt=F('away_score'))) |
                    Q(Q(away_team=team) & Q(away_score__lt=F('home_score'))))\
        .count()
        return won

    def get_points(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(Q(home_team=team) & Q(home_score__gt=F('away_score'))) |
                    Q(Q(away_team=team) & Q(away_score__gt=F('home_score'))))\
        .count()
        draw = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(Q(home_team=team) | Q(away_team=team)) &
                    Q(home_score=F('away_score')))\
            .count()
        return (won * 3) + draw

    def get_home_played(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        played = models.Match.historical\
            .filter(league_season=obj)\
            .filter(home_team=team)\
            .count()
        return played

    def get_home_won(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(home_team=team) & Q(home_score__gt=F('away_score')))\
        .count()
        return won

    def get_home_draw(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        draw = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(home_team=team) &
                    Q(home_score=F('away_score')))\
            .count()
        return draw

    def get_home_lost(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(home_team=team) & Q(home_score__lt=F('away_score')))\
        .count()
        return won

    def get_home_points(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(home_team=team) & Q(home_score__gt=F('away_score')))\
        .count()
        draw = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(home_team=team) &
                    Q(home_score=F('away_score')))\
            .count()
        return (won * 3) + draw

    def get_away_played(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        played = models.Match.historical\
            .filter(league_season=obj)\
            .filter(away_team=team)\
            .count()
        return played

    def get_away_won(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(away_team=team) & Q(away_score__gt=F('home_score')))\
        .count()
        return won

    def get_away_draw(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        draw = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(away_team=team) &
                    Q(home_score=F('away_score')))\
            .count()
        return draw

    def get_away_lost(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(away_team=team) & Q(home_score__gt=F('away_score')))\
        .count()
        return won

    def get_away_points(self, obj):
        team_pk = self.context['team_pk']
        team = models.Team.objects.get(pk=team_pk)
        won = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(away_team=team) & Q(home_score__lt=F('away_score')))\
        .count()
        draw = models.Match.historical\
            .filter(league_season=obj)\
            .filter(Q(away_team=team) &
                    Q(home_score=F('away_score')))\
            .count()
        return (won * 3) + draw

    class Meta:
        model = models.LeagueSeason


class MatchSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    league_season = LeagueSeasonSerializer(many=False)
    home_team = TeamSerializer()
    away_team = TeamSerializer()

    class Meta:
        model = models.Match


class MatchPlayerSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    match = MatchSerializer()

    class Meta:
        model = models.MatchPlayer

class MatchPlayerDetailPlayerSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    player = PlayerSerializer()

    class Meta:
        model = models.MatchPlayer
