from django_filters import (FilterSet, CharFilter)

from ..models import (Team, Player)


class TeamFilter(FilterSet):
    name = CharFilter(lookup_type='icontains')
    country__name = CharFilter(lookup_type='icontains')

    class Meta:
        model = Team
        fields = ['name', 'country__name']


class PlayerFilter(FilterSet):
    current_team__name = CharFilter(lookup_type='icontains')
    nationality__name = CharFilter(lookup_type='icontains')

    class Meta:
        model = Player
        fields = []