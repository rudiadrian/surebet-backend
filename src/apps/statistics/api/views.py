from datetime import date

from django.db.models import Q
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import detail_route
from rest_framework.filters import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT
from isoweek import Week

from . import  serializers
from .. import models
from apps.analyses.api.serializers import MatchAnalyseSerializer
from . filters import (TeamFilter, PlayerFilter)
from surebet.api.permissions import IsAdminOrReadOnly
from libs.mixins import CacheMixin


class CountryViewSet(viewsets.ModelViewSet):
    queryset = models.Country.objects.all()
    serializer_class = serializers.CountrySerializer
    permission_classes = [permissions.IsAuthenticated, IsAdminOrReadOnly]

    @detail_route(permission_classes=[permissions.IsAuthenticated, IsAdminOrReadOnly])
    def players(self, request, pk):
        country = self.get_object()
        players = models.Player.objects.filter(nationality=country)
        serializer = serializers.PlayerSerializer(players,
                                                  context={'request': request},
                                                  many=True)
        return Response(serializer.data)

    @detail_route(permission_classes=[permissions.IsAuthenticated, IsAdminOrReadOnly])
    def teams(self, request, pk):
        country = self.get_object()
        teams = models.Team.objects.filter(country=country)
        serializer = serializers.TeamSerializer(teams,
                                                context={'request': request},
                                                many=True)
        return Response(serializer.data)

    @detail_route()
    def leagues_seasons(self, request, pk):
        country = self.get_object()
        leagues = country.leagues
        serializer = serializers.CountryLeagueSerializer(leagues,
                                                         context={'request': request},
                                                         many=True)
        return Response(serializer.data)


class SeasonViewSet(viewsets.ModelViewSet):
    queryset = models.Season.objects.all()
    serializer_class = serializers.SeasonSerializer
    permission_classes = [permissions.IsAuthenticated, IsAdminOrReadOnly]


class LeagueViewSet(viewsets.ModelViewSet):
    queryset = models.League.objects.all()
    serializer_class = serializers.LeagueDetailSerializer
    permission_classes = [permissions.IsAuthenticated, IsAdminOrReadOnly]

    @detail_route(permission_classes=[permissions.IsAuthenticated, IsAdminOrReadOnly])
    def seasons(self, request, pk):
        league = self.get_object()

        seasons = league.seasons
        serializer = serializers.SeasonSerializer(seasons,
                                                  context={'request': request},
                                                  many=True)
        return Response(serializer.data)


class LeagueSeasonViewSet(CacheMixin, viewsets.ModelViewSet):
    queryset = models.LeagueSeason.objects.all()
    serializer_class = serializers.LeagueSeasonSerializer
    permission_classes = [permissions.IsAuthenticated, IsAdminOrReadOnly]
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('league__id', 'season__id')

    @detail_route()
    def standings(self, request, pk):
        league_season = self.get_object()
        teams = models.Team.objects\
            .filter(Q(home_matches__league_season=league_season) |
                    Q(away_matches__league_season=league_season))\
            .distinct()
        serializer = serializers.TeamStandingsSerializer(teams,
                                                         context={'request': request,
                                                                  'league_season_pk': league_season.pk},
                                                         many=True)
        return Response(serializer.data)


class TeamViewSet(viewsets.ModelViewSet):
    queryset = models.Team.objects.all()
    serializer_class = serializers.TeamSerializer
    permission_classes = [IsAdminOrReadOnly]
    paginate_by = 20

    def get_queryset(self):
        query_set = models.Team.objects.all()
        name = self.request.QUERY_PARAMS.get('name', None)
        if name:
            query_set = query_set.filter(name__icontains=name)
        country = self.request.QUERY_PARAMS.get('country', None)
        if country:
            query_set = query_set.filter(country__name__icontains=country)
        return query_set

    def dispatch(self, request, *args, **kwargs):
        return super(TeamViewSet, self).dispatch(request, *args, **kwargs)

    @detail_route(permission_classes=[permissions.IsAuthenticated, IsAdminOrReadOnly])
    def players(self, request, pk):
        team = self.get_object()
        players = models.Player.objects.filter(current_team=team)
        serializer = serializers.TeamPlayerSerializer(players,
                                                  context={'request': request},
                                                  many=True)
        return Response(serializer.data)

    @detail_route()
    def latest_matches(self, request, pk=None):
        team = self.get_object()
        matches = models.Match.historical\
            .filter(Q(home_team=team) | Q(away_team=team))\
            .order_by('-datetime')[:5]
        serializer = serializers.MatchSerializer(matches,
                                                 context={'request': request},
                                                 many=True)
        return Response(serializer.data)

    @detail_route()
    def last_home_matches(self, request, pk=None):
        team = self.get_object()
        matches = models.Match.historical\
            .filter(home_team=team)\
            .order_by('-datetime')[:5]
        serializer = serializers.MatchSerializer(matches,
                                                 context={'request': request},
                                                 many=True)
        return Response(serializer.data)

    @detail_route()
    def last_away_matches(self, request, pk=None):
        team = self.get_object()
        matches = models.Match.historical\
            .filter(away_team=team)\
            .order_by('-datetime')[:5]
        serializer = serializers.MatchSerializer(matches,
                                                 context={'request': request},
                                                 many=True)
        return Response(serializer.data)

    @detail_route()
    def competitions(self, request, pk=None):
        team = self.get_object()
        today = date.today()
        league_seasons = models.LeagueSeason.objects\
            .filter(start_date__lte=today)\
            .filter(end_date__gte=today)\
            .filter(Q(matches__home_team=team) | Q(matches__away_team=team))\
            .distinct()
        serializer = serializers.LeagueSeasonTeamSerializer(
                                                        league_seasons,
                                                        many=True,
                                                        context=
                                                        {'request': request,
                                                         'team_pk': team.pk})
        return Response(serializer.data)


class PlayerViewSet(viewsets.ModelViewSet):
    queryset = models.Player.objects.all()
    serializer_class = serializers.PlayerSerializer
    permission_classes = [IsAdminOrReadOnly]
    paginate_by = 20

    def get_queryset(self):
        query_set = models.Player.objects.all()
        full_name = self.request.QUERY_PARAMS.get('full_name', None)
        if full_name:
            query_set = query_set.extra(
                where=["LOWER(CONCAT(first_name, ' ', last_name)) LIKE %s"],
                params=['%{full_name}%'.format(full_name=full_name.lower())])
        current_team = self.request.QUERY_PARAMS.get('current_team', None)
        if current_team:
            query_set = query_set.filter(
                current_team__name__icontains=current_team)
        nationality = self.request.QUERY_PARAMS.get('nationality', None)
        if nationality:
            query_set = query_set.filter(
                nationality__name__icontains=nationality)
        return query_set

    @detail_route()
    def matches(self, request, pk=None):
        player = self.get_object()
        queryset = player.matches.all()
        serializer = serializers.MatchSerializer(queryset,
                                                 many=True,
                                                 context={'request': request})
        return Response(serializer.data)

    @detail_route()
    def last_matches(self, request, pk=None):
        player = self.get_object()
        queryset = models.MatchPlayer.objects\
                       .filter(player=player)\
                       .order_by('-match__datetime')[:5]
        serializer = serializers.MatchPlayerSerializer(queryset,
                                                 many=True,
                                                 context={'request': request})
        return Response(serializer.data)

    @detail_route()
    def current_competitions(self, request, pk=None):
        player = self.get_object()
        today = date.today()
        league_seasons = models.LeagueSeason.objects\
            .filter(start_date__lte=today)\
            .filter(end_date__gte=today)\
            .filter(matches__players=player)\
            .distinct()
        serializer = serializers.LeagueSeasonPlayerSerializer(league_seasons,
                                                              many=True,
                                                              context=
                                                              {'request': request,
                                                               'player_pk': player.pk})
        return Response(serializer.data)


class MatchViewSet(CacheMixin, viewsets.ModelViewSet):
    queryset = models.Match.objects.all()
    serializer_class = serializers.MatchSerializer
    paginate_by = 30
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ['league_season',]

    def get_queryset(self):
        query_set = models.Match.objects.all()
        week = self.request.QUERY_PARAMS.get('week', None)
        year = self.request.QUERY_PARAMS.get('year', None)
        if week and year:
            iso_week = Week(int(year), int(week))
            query_set = query_set.filter(Q(datetime__gte=iso_week.monday()) &
                                         Q(datetime__lte=iso_week.sunday()))
        return query_set

    @detail_route()
    def home_players(self, request, pk):
        match = self.get_object()
        players = match.players_matches_set.filter(side='home')
        serializer = serializers.MatchPlayerDetailPlayerSerializer(players,
                                                                   context=
                                                                   {'request': request},
                                                                   many=True)
        return Response(serializer.data)

    @detail_route()
    def away_players(self, request, pk):
        match = self.get_object()
        players = match.players_matches_set.filter(side='away')
        serializer = serializers.MatchPlayerDetailPlayerSerializer(players,
                                                                   context=
                                                                   {'request': request},
                                                                   many=True)
        return Response(serializer.data)

    @detail_route()
    def previous_matches(self, request, pk):
        match = self.get_object()
        matches = models.Match.historical\
            .filter(Q(Q(home_team=match.home_team) & Q(away_team=match.away_team)) |
                    Q(Q(home_team=match.away_team) & Q(away_team=match.home_team)))
        serializer = serializers.MatchSerializer(matches,
                                                 context={'request': request},
                                                 many=True)
        return Response(serializer.data)

    @detail_route()
    def analyse(self, request, pk):
        match = self.get_object()
        if hasattr(match, 'matchanalyse'):
            analysis = match.matchanalyse
            serializer = MatchAnalyseSerializer(analysis,
                                                context={'request': request},
                                                many=False)
            return Response(serializer.data)
        else:
            return Response(status=HTTP_204_NO_CONTENT)



class MatchPlayerViewSet(viewsets.ModelViewSet):
    queryset = models.MatchPlayer.objects.all()
    serializer_class = serializers.MatchPlayerSerializer
    permission_classes = [permissions.IsAuthenticated, IsAdminOrReadOnly]
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ['side', 'player']

    def get_queryset(self):
        queryset = models.MatchPlayer.objects.all()
        competition_id = self.request.QUERY_PARAMS.get('competition_id', None)
        limit = self.request.QUERY_PARAMS.get('limit', None)
        if competition_id is not None:
            queryset = queryset.filter(match__league_season__season_id=
                                       competition_id)
        return queryset
