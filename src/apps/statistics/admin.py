from django.contrib import admin

from . import models


class TeamAdmin(admin.ModelAdmin):
    list_filter = ('country',)


class PlayerAdmin(admin.ModelAdmin):
    list_filter = ('current_team', 'nationality',)


class MatchAdmin(admin.ModelAdmin):
    list_filter = ('home_team', 'away_team', 'league_season')


admin.site.register(models.Country)
admin.site.register(models.Season)
admin.site.register(models.League)
admin.site.register(models.LeagueSeason)
admin.site.register(models.LeagueSeasonCrawlerData)
admin.site.register(models.Team, TeamAdmin)
admin.site.register(models.Player, PlayerAdmin)
admin.site.register(models.Match, MatchAdmin)
admin.site.register(models.MatchPlayer)