from django.db import models

SIDE_CHOICES = (
    ('home', 'Home'),
    ('away', 'Away')
)


class Country(models.Model):
    name = models.CharField(max_length=75)
    slug = models.CharField(max_length=75)

    class Meta:
        verbose_name_plural = 'Countries'

    def __unicode__(self):
        return self.name

class Season(models.Model):
    start_year = models.PositiveSmallIntegerField()
    end_year = models.PositiveSmallIntegerField()

    def __unicode__(self):
        if self.start_year != self.end_year:
            return u'{0}/{1}'.format(self.start_year, self.end_year)
        else:
            return self.start_year


class League(models.Model):
    name = models.CharField(max_length=35)
    slug = models.SlugField(max_length=35)
    country = models.ForeignKey(Country, related_name='leagues')
    seasons = models.ManyToManyField(Season,
                                     through='LeagueSeason',
                                     through_fields=('league', 'season'))

    def __unicode__(self):
        return self.name


class LeagueSeason(models.Model):
    league = models.ForeignKey(League)
    season = models.ForeignKey(Season)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)

    def __unicode__(self):
        return u'{0} {1}'.format(self.league, self.season)


class LeagueSeasonCrawlerData(models.Model):
    league_season = models.OneToOneField(LeagueSeason)
    url = models.URLField()
    historical_scraped = models.BooleanField(default=False)

    def __unicode__(self):
        return u'{league_season}'.format(league_season=self.league_season)


class Team(models.Model):
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=50)
    country = models.ForeignKey(Country, related_name='teams')
    whoscored_id = models.PositiveIntegerField(unique=True)

    def __unicode__(self):
        return self.name


class Player(models.Model):
    first_name = models.CharField(max_length=35)
    last_name = models.CharField(max_length=35, blank=True, null=True)
    nationality = models.ForeignKey(Country, related_name='players')
    current_team = models.ForeignKey(Team, related_name='players')
    age = models.PositiveSmallIntegerField()
    position = models.CharField(max_length=5)
    whoscored_id = models.PositiveIntegerField(unique=True)

    def get_full_name(self):
        if self.last_name:
            return u' '.join((self.first_name, self.last_name))
        else:
            return self.first_name

    def __unicode__(self):
        return u' | '.join((self.get_full_name(), self.current_team.__unicode__()))


class HistoricalMatchManager(models.Manager):
    def get_queryset(self):
        return super(HistoricalMatchManager, self)\
            .get_queryset().filter(home_score__isnull=False,
                                    away_score__isnull=False)


class UpcomingMatchManager(models.Manager):
    def get_queryset(self):
        return super(HistoricalMatchManager, self)\
            .get_queryset().filter(home_score__isnull=True,
                                    away_score__isnull=True)


class Match(models.Model):
    home_score = models.PositiveSmallIntegerField(blank=True, null=True)
    away_score = models.PositiveSmallIntegerField(blank=True, null=True)
    datetime = models.DateTimeField()
    home_team = models.ForeignKey(Team, related_name='home_matches')
    away_team = models.ForeignKey(Team, related_name='away_matches')
    league_season = models.ForeignKey(LeagueSeason, related_name='matches')
    players = models.ManyToManyField(Player,
                                          through='MatchPlayer',
                                          through_fields=('match', 'player'),
                                          related_name='matches')
    whoscored_id = models.PositiveIntegerField(unique=True)
    objects = models.Manager()
    historical = HistoricalMatchManager()
    upcoming = UpcomingMatchManager()

    class Meta:
        verbose_name_plural = 'Matches'
        index_together = [
            ['home_team', 'away_team']
        ]

    def __unicode__(self):
        return u'{home} {home_score}:{away_score} {away} | {league_season}'\
            .format(home=self.home_team.__unicode__(),
                    home_score=self.home_score,
                    away_score=self.away_score,
                    away=self.away_team.__unicode__(),
                    league_season=self.league_season.__unicode__())


class MatchPlayer(models.Model):
    match = models.ForeignKey(Match, related_name='players_matches_set')
    player = models.ForeignKey(Player, related_name='matches_players_set')
    rating = models.FloatField()
    goals = models.PositiveSmallIntegerField(default=0)
    side = models.CharField(max_length=4, choices=SIDE_CHOICES)

    def __unicode__(self):
        return u'{player} | {match} | {side}'.format(player=self.player,
                                           match=self.match.__unicode__(),
                                           side=self.side)