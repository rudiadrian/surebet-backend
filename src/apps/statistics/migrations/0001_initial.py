# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=75)),
                ('slug', models.CharField(max_length=75)),
            ],
            options={
                'verbose_name_plural': 'Countries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='League',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=35)),
                ('slug', models.SlugField(max_length=35)),
                ('country', models.ForeignKey(related_name='leagues', to='statistics.Country')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LeagueSeason',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.DateField(null=True, blank=True)),
                ('end_date', models.DateField(null=True, blank=True)),
                ('league', models.ForeignKey(to='statistics.League')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('home_score', models.PositiveSmallIntegerField()),
                ('away_score', models.PositiveSmallIntegerField()),
                ('datetime', models.DateTimeField()),
                ('whoscored_id', models.PositiveIntegerField(unique=True)),
            ],
            options={
                'verbose_name_plural': 'Matches',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MatchPlayer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.FloatField()),
                ('goals', models.PositiveSmallIntegerField(default=0)),
                ('side', models.CharField(max_length=4, choices=[(b'home', b'Home'), (b'away', b'Away')])),
                ('match', models.ForeignKey(related_name='players_matches_set', to='statistics.Match')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=35)),
                ('last_name', models.CharField(max_length=35, null=True, blank=True)),
                ('age', models.PositiveSmallIntegerField()),
                ('position', models.CharField(max_length=5)),
                ('whoscored_id', models.PositiveIntegerField(unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Season',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_year', models.PositiveSmallIntegerField()),
                ('end_year', models.PositiveSmallIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('slug', models.CharField(max_length=50)),
                ('whoscored_id', models.PositiveIntegerField(unique=True)),
                ('country', models.ForeignKey(related_name='teams', to='statistics.Country')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='player',
            name='current_team',
            field=models.ForeignKey(related_name='players', to='statistics.Team'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='nationality',
            field=models.ForeignKey(related_name='players', to='statistics.Country'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='matchplayer',
            name='player',
            field=models.ForeignKey(related_name='matches_players_set', to='statistics.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='match',
            name='away_team',
            field=models.ForeignKey(related_name='away_matches', to='statistics.Team'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='match',
            name='home_team',
            field=models.ForeignKey(related_name='home_matches', to='statistics.Team'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='match',
            name='league_season',
            field=models.ForeignKey(related_name='matches', to='statistics.LeagueSeason'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='match',
            name='players',
            field=models.ManyToManyField(related_name='matches', through='statistics.MatchPlayer', to='statistics.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='leagueseason',
            name='season',
            field=models.ForeignKey(to='statistics.Season'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='league',
            name='seasons',
            field=models.ManyToManyField(to='statistics.Season', through='statistics.LeagueSeason'),
            preserve_default=True,
        ),
    ]
