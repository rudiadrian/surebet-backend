# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0001_initial'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='match',
            index_together=set([('home_team', 'away_team')]),
        ),
    ]
