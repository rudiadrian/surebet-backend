# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0002_auto_20150106_2039'),
    ]

    operations = [
        migrations.CreateModel(
            name='LeagueSeasonCrawlerData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('historical_scraped', models.BooleanField(default=False)),
                ('league_season', models.OneToOneField(to='statistics.LeagueSeason')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
