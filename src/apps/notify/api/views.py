from django.utils import timezone
from notifications.models import Notification
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK,
                                   HTTP_204_NO_CONTENT)
import gevent

from . import serializers


class TimeoutPolling(Exception):
    pass


class NotificationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = serializers.NotificationSerializer
    permission_classes = [permissions.IsAuthenticated]

    @list_route()
    def unread(self, request):
        user = request.user
        notifications = user.notifications.unread()
        serializer = serializers.NotificationSerializer(notifications,
                                                        context={'request': request},
                                                        many=True)
        return Response(serializer.data)

    @list_route()
    def read(self, request):
        user = request.user
        notifications = user.notifications.read()
        serializer = serializers.NotificationSerializer(notifications,
                                                        context={'request': request},
                                                        many=True)
        return Response(serializer.data)

    @list_route()
    def mark_all_as_read(self, request):
        user = request.user
        user.notifications.mark_all_as_read()
        return Response({'success': 'Notifications have been successfully marked as read.'})

    @list_route()
    def mark_all_as_unread(self, request):
        user = request.user
        user.notifications.mark_all_as_unread()
        return Response({'success': 'Notifications have been successfully marked as unread.'}
                        , status=meHTTP_200_OK)

    @detail_route()
    def mark_as_read(self, request, pk=None):
        notification = self.get_object()
        notification.mark_as_read()
        return Response({'success': 'Notification has been successfully marked as read.'}
                        , status=HTTP_200_OK)

    @detail_route()
    def mark_as_unread(self, request, pk=None):
        notification = self.get_object()
        notification.mark_as_unread()
        return Response({'success': 'Notification has been successfully marked as unread.'}
                        , status=HTTP_200_OK)

    @list_route()
    def latest(self, request):
        user = request.user
        notifications = user.notifications.all()[:5]
        serializer = serializers.NotificationSerializer(notifications,
                                                        context={'request': request},
                                                        many=True)
        return Response(serializer.data)

    @list_route()
    def watch_notifications(self, request):
        now = timezone.now()
        timeout = gevent.Timeout(seconds=60, exception=TimeoutPolling)
        timeout.start()
        try:
            while True:
                new_notifications = Notification.objects.filter(timestamp__gte=now)
                if new_notifications.exists():
                    serializer = serializers.NotificationSerializer(new_notifications,
                                                                    context={'request': request},
                                                                    many=True)
                    return Response(serializer.data)
                else:
                    gevent.sleep(seconds=5)
        except TimeoutPolling:
            return Response(status=HTTP_204_NO_CONTENT)
        finally:
            timeout.cancel()

    def get_queryset(self):
        user = self.request.user
        return user.notifications