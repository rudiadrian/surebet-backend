from rest_framework import serializers
from rest_framework.reverse import reverse

from notifications.models import Notification


class NotificationSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    links = serializers.SerializerMethodField()

    def get_links(self, obj):
        request = self.context['request']
        return {
            'self': reverse('notification-detail',
                            kwargs={'pk': obj.pk},
                            request=request),
            'unread': reverse('notification-unread', request=request),
            'read': reverse('notification-read', request=request),
            'mark_all_as_read': reverse('notification-mark-all-as-read',
                                        request=request),
            'mark_all_as_unread': reverse('notification-mark-all-as-unread',
                                        request=request),
            'mark_as_read': reverse('notification-mark-as-read',
                                    kwargs={'pk': obj.pk},
                                    request=request),
            'mark_as_unread': reverse('notification-mark-as-unread',
                                      kwargs={'pk': obj.pk},
                                      request=request),
            'latest': reverse('notification-latest',
                               request=request)
        }

    class Meta:
        model = Notification