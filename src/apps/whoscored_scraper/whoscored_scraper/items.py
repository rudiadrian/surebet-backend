# -*- coding: utf-8 -*-
from scrapy import (Item, Field)

class MatchItem(Item):
    whoscored_id = Field()
    datetime = Field()
    home_whoscored_id = Field()
    home_name = Field()
    away_whoscored_id = Field()
    away_name = Field()
    final_result = Field()
    league_whoscored_id = Field()
    league = Field()
    season = Field()
    home_team_players = Field()
    away_team_players = Field()
    full_time = Field()