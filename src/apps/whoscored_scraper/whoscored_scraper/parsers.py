from datetime import datetime
import re
import json

from pip._vendor import requests
from scrapy.selector import Selector

from .items import MatchItem
from .constants import (
    FIXTURE_DATETIME_FORMAT, FixtureDataField)


def _get_cleared_scrapped_value(value):
    return value.strip().replace(' ', '').replace('*', '')


def _get_fixture_datetime(date, time):
    datetime_string = '{date}, {time}'.format(date=date, time=time)
    return datetime.strptime(datetime_string, FIXTURE_DATETIME_FORMAT)


def get_match_item_from_match_data(match_data):
    match = MatchItem()

    match['whoscored_id'] = match_data[FixtureDataField.id.value]
    match['datetime'] = _get_fixture_datetime(
                date=match_data[FixtureDataField.date.value],
                time=match_data[FixtureDataField.time.value])
    match['home_whoscored_id'] = match_data[FixtureDataField.home_id.value]
    match['home_name'] = match_data[FixtureDataField.home.value]
    match['away_whoscored_id'] = match_data[FixtureDataField.away_id.value]
    match['away_name'] = match_data[FixtureDataField.away.value]
    match['final_result'] = _get_cleared_scrapped_value(match_data[FixtureDataField.result.value])
    match['full_time'] = match_data[FixtureDataField.full_time.value]

    return match


def get_season_info_from_response(response):
    selector = Selector(response=response)
    season_info = {}

    league_element = selector.xpath('//*[@id="breadcrumb-nav"]/a')
    league_whoscored_id = int(re.findall('Tournaments\/(\d*)\/Seasons',
                                         league_element.xpath('@href').extract()[0])[0])
    league, season = league_element.xpath('text()').extract()[0].split('-', 1)

    season_info['league_whoscored_id'] = league_whoscored_id
    season_info['league'] = league.strip()
    season_info['season'] = season.strip()

    return season_info


def _get_players_page_response(team_whoscored_id, match_whoscored_id):
    url = 'http://www.whoscored.com/StatisticsFeed/1/GetMatchCentrePlayerStatistics'
    params = {
        'isCurrent': 'true',
        'category': 'summary',
        'subcategory': 'all',
        'statsAccumulationType': '0',
        'teamIds': team_whoscored_id,
        'matchId': match_whoscored_id
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'Host': 'www.whoscored.com',
        'Referer': 'http://www.whoscored.com/Matches/{match_id}/LiveStatistics/'.format(match_id=match_whoscored_id)
    }
    return requests.get(url, params=params, headers=headers)


def _get_players_from_json(response):
    players = []
    data = response['playerTableStats']
    for player in data:
        if player['positionText'] != 'Sub':
            goals = 0
            for incident in player['incidents']:
                if incident['type']['displayName'] == 'Goal':
                    goals += 1
            players.append({'name': player['name'],
                            'rating': player['rating'],
                            'position': player['positionText'],
                            'region_code': player['regionCode'],
                            'age': player['age'],
                            'goals': goals,
                            'whoscored_id': player['playerId']})
    return players


def get_players_from_match(team_whoscored_id, match_whoscored_id):
    response = _get_players_page_response(team_whoscored_id, match_whoscored_id)
    return _get_players_from_json(json.loads(response.text.strip()))