from ast import literal_eval
from datetime import date

from scrapy.contrib.spiders.crawl import CrawlSpider
from scrapy.http.request import Request
from apps.statistics.models import LeagueSeasonCrawlerData

from ..parsers import (get_match_item_from_match_data,
                       get_season_info_from_response,
                       get_players_from_match)


class HistoricalCrawler(CrawlSpider):
    name = 'historical_crawler'
    allowed_domains = ['www.whoscored.com']

    def __init__(self, *args, **kwargs):
        super(HistoricalCrawler, self).__init__(*args, **kwargs)
        self.crawler_data_list = LeagueSeasonCrawlerData.objects\
            .filter(historical_scraped=False)
        self.start_urls = [data.url for data in self.crawler_data_list]

    def _get_cleared_data(self, value):
        return value\
            .strip()\
            .replace('\n', '')\
            .replace('\r', '')\
            .replace(',,', ",'',")

    def parse(self, response):
        for data in self.crawler_data_list:
            if data.url == response.request.url:
                start_year = data.league_season.season.start_year
                end_year = data.league_season.season.end_year
                break
        for year in xrange(start_year, end_year+1):
            weeks = int(date(year, 12, 31).strftime('%W'))
            for week in xrange(1, weeks+1):
                url = ''.join([response.request.url,
                               '?d={year}W{week}&isAggregate=false'.format(year=year, week=week)])
                yield Request(url=url, callback=self.parse_fixtures)

    def parse_fixtures(self, response):
        if response.body_as_unicode().strip() == '[]':
            return
        try:
            matches_data = literal_eval(self._get_cleared_data(response.body))
        except (ValueError, SyntaxError), e:
            pass #Log later
        else:
            for data in matches_data:
                item = get_match_item_from_match_data(data)
                if item['full_time'] != 'FT':
                    return
                url = 'http://www.whoscored.com/Matches/{match_id}'\
                    .format(match_id=item['whoscored_id'])
                request = Request(url=url, callback=self.parse_match)
                request.meta['item'] = item
                yield request

    def parse_match(self, response):
        item = response.meta['item']
        season_info  = get_season_info_from_response(response)

        item['league_whoscored_id'] = season_info['league_whoscored_id']
        item['league'] = season_info['league']
        item['season'] = season_info['season']

        url = 'http://www.whoscored.com/Matches/{match_id}/LiveStatistics/'\
            .format(match_id=item['whoscored_id'])
        request = Request(url=url, callback=self.parse_players)
        request.meta['item'] = item
        yield request

    def parse_players(self, response):
        item = response.meta['item']

        item['home_team_players'] = get_players_from_match(item['home_whoscored_id'],
                                                           item['whoscored_id'])
        item['away_team_players'] = get_players_from_match(item['away_whoscored_id'],
                                                           item['whoscored_id'])

        yield item