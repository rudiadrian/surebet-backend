# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from django.core.exceptions import ObjectDoesNotExist
import pycountry

from spiders.historicalcrawler import HistoricalCrawler
from spiders.upcomingcrawler import UpcomingCrawler
from apps.statistics.models import (League, LeagueSeason, Team,
                                    Match, Country, Player, MatchPlayer)


def get_slug(text):
    return text.lower().replace(' ', '_')


class HistoricalItemSavePipeline(object):

    def _create_player(self, player, team, side, match):
        region_code = player['region_code'].upper()
        name = player['name'].split(' ', 2)
        position = player['position']
        age = player['age']
        player_id = player['whoscored_id']
        if len(name) == 1:
            name.append('Blank')
        if len(region_code) == 2:
            country_name = pycountry.countries.get(alpha2=region_code).name
        elif len(region_code) == 3:
            country_name = pycountry.countries.get(alpha3=region_code).name
        else:
            country_name = pycountry.subdivisions.get(code=region_code).name
        country, created = Country.objects.get_or_create(name=country_name,
                                                         slug=get_slug(country_name))
        try:
            new_player = Player.objects.get(whoscored_id=player_id)
            new_player.current_team = team
            new_player.position = position
            new_player.save()
        except ObjectDoesNotExist:
            new_player, created = Player.objects.get_or_create(first_name=name[0],
                                                           last_name=name[1],
                                                           nationality=country,
                                                           current_team=team,
                                                           position=position,
                                                           age=age,
                                                           whoscored_id=player_id)
        return new_player

    def process_item(self, item, spider):
        league = League.objects.get(slug=get_slug(item['league']))
        season_start_year, season_end_year = item['season'].split('/', 2)
        home_score, away_score = item['final_result'].split(':', 2)
        league_season = LeagueSeason.objects.get(league=league,
                                                 season__start_year=season_start_year,
                                                 season__end_year=season_end_year)
        home_team, created = Team.objects.get_or_create(name=item['home_name'],
                                                        slug=get_slug(item['home_name']),
                                                        country=league.country,
                                                        whoscored_id=item['home_whoscored_id'])
        away_team, created = Team.objects.get_or_create(name=item['away_name'],
                                                        slug=get_slug(item['away_name']),
                                                        country=league.country,
                                                        whoscored_id=item['away_whoscored_id'])
        match, created = Match.objects.get_or_create(home_score=home_score,
                                                     away_score=away_score,
                                                     datetime=item['datetime'],
                                                     home_team=home_team,
                                                     away_team=away_team,
                                                     league_season=league_season,
                                                     whoscored_id=item['whoscored_id'])

        for player in item['home_team_players']:
            home_player = None
            if not player['rating'] or not player['region_code']:
                continue
            home_player = self._create_player(player, home_team, 'home', match)
            match_player, created = MatchPlayer.objects.get_or_create(player=home_player,
                                                                  match=match,
                                                                  rating=player['rating'],
                                                                  goals=player['goals'],
                                                                  side='home')

        for player in item['away_team_players']:
            away_player = None
            if not player['rating'] or not player['region_code']:
                continue
            away_player = self._create_player(player, away_team, 'away', match)
            match_player, created = MatchPlayer.objects.get_or_create(player=away_player,
                                                                  match=match,
                                                                  rating=player['rating'],
                                                                  goals=player['goals'],
                                                                  side='away')


class UpcomingItemSavePipeline(object):

    def process_item(self, item, spider):
        league = League.objects.get(slug=get_slug(item['league']))
        season_start_year, season_end_year = item['season'].split('/', 2)
        league_season = LeagueSeason.objects.get(league=league,
                                                 season__start_year=season_start_year,
                                                 season__end_year=season_end_year)
        home_team, created = Team.objects.get_or_create(name=item['home_name'],
                                                        slug=get_slug(item['home_name']),
                                                        country=league.country,
                                                        whoscored_id=item['home_whoscored_id'])
        away_team, created = Team.objects.get_or_create(name=item['away_name'],
                                                        slug=get_slug(item['away_name']),
                                                        country=league.country,
                                                        whoscored_id=item['away_whoscored_id'])
        match, created = Match.objects.get_or_create(datetime=item['datetime'],
                                                     home_team=home_team,
                                                     away_team=away_team,
                                                     league_season=league_season,
                                                     whoscored_id=item['whoscored_id'])


class ItemSavePipeline(object):
    def factory(self, spider):
        if isinstance(spider, HistoricalCrawler):
            return HistoricalItemSavePipeline()
        elif isinstance(spider, UpcomingCrawler):
            return UpcomingItemSavePipeline()
        else:
            raise NotImplementedError('No item save pipeline for that spider.')


class DjangoItemPipeline(object):
    def process_item(self, item, spider):
        item_save_pipeline = ItemSavePipeline().factory(spider)
        item_save_pipeline.process_item(item, spider)