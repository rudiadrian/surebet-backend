# -*- coding: utf-8 -*-

# Scrapy settings for whoscored_scraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
import sys
sys.path.insert(0, '/home/adrian/dev/surebet-backend/src')

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings.dev'

import django
django.setup()

BOT_NAME = 'whoscored_scraper'

SPIDER_MODULES = ['whoscored_scraper.spiders']
NEWSPIDER_MODULE = 'whoscored_scraper.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'whoscored_scraper (+http://www.yourdomain.com)'

DOWNLOAD_DELAY = 0.4

ITEM_PIPELINES = {
    'whoscored_scraper.pipelines.DjangoItemPipeline': 300,
}