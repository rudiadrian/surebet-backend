from enum import Enum,    unique

FIXTURE_DATETIME_FORMAT = '%A, %b %d %Y, %H:%M'
MATCH_DATETIME_FORMAT = '%d-%m-%Y, %H:%M'

@unique
class FixtureDataField(Enum):
    id = 0
    date = 2
    time = 3
    home_id = 4
    home = 5
    away_id = 7
    away = 8
    result = 10
    full_time = 14


@unique
class MatchDataField(Enum):
    id = 0
    date = 2
    time = 3
    home_id = 4
    home = 5
    away_id = 7
    away = 8
    result = 10
    full_time = 14
    season = 15
    league = 16
