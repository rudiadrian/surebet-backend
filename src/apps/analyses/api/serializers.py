from rest_framework import serializers

from ..models import (Analyse, MatchAnalyse, DataRow)
from apps.statistics.api.serializers import (MatchSerializer)


class DataRowSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = DataRow


class AnalyseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Analyse


class MatchAnalyseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    match = MatchSerializer(many=False)

    class Meta:
        model = MatchAnalyse