from rest_framework.decorators import list_route
from rest_framework.filters import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import (ReadOnlyModelViewSet)
from libs.mixins import CacheMixin

from ..models import (DataRow, Analyse, MatchAnalyse)
from .serializers import (AnalyseSerializer, MatchAnalyseSerializer,
                          DataRowSerializer)


class DataRowViewSet(ReadOnlyModelViewSet):
    queryset = DataRow.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = DataRowSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ['data_set__analysis']
    paginate_by = 20

    def get_queryset(self):
        return DataRow.objects.all()


class AnalyseViewSet(ReadOnlyModelViewSet):
    queryset = Analyse.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = AnalyseSerializer

    def get_queryset(self):
        return Analyse.objects.all()


class MatchAnalyseViewSet(ReadOnlyModelViewSet):
    queryset = MatchAnalyse.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = MatchAnalyseSerializer
    paginate_by = 20

    def get_queryset(self):
        if self.request.user.userprofile.classifier is not None:
            analyse = self.request.user.userprofile.classifier
        else:
            analyse = Analyse.objects.get(slug='neural_network')
        query_set = MatchAnalyse.objects.filter(match__home_score__isnull=True,
                                                match__away_score__isnull=True,
                                                analyse=analyse)
        home_team = self.request.QUERY_PARAMS.get('home', None)
        if home_team is not None:
            query_set = query_set\
                .filter(match__home_team__name__icontains=home_team)
        away_team = self.request.QUERY_PARAMS.get('away', None)
        if away_team is not None:
            query_set = query_set\
                .filter(match__away_team__name__icontains=away_team)
        league = self.request.QUERY_PARAMS.get('league', None)
        if league is not None:
            query_set = query_set\
                .filter(match__league_season__league__name__icontains=league)
        return query_set

    @list_route()
    def highest_outcomes(self, request):
        if self.request.user.userprofile.classifier is not None:
            analyse = self.request.user.userprofile.classifier
        else:
            analyse = Analyse.objects.get(slug='neural_network')
        query_set = MatchAnalyse.objects\
            .filter(match__home_score__isnull=True,
                    match__away_score__isnull=True,
                    analyse=analyse)\
            .extra(select={'highest_outcome': "GREATEST(home_win_outcome, draw_outcome, away_win_outcome)"})\
            .extra(order_by=['-highest_outcome'])[:5]
        serializer = MatchAnalyseSerializer(query_set,
                                            context={'request': request},
                                            many=True)
        return Response(serializer.data)

