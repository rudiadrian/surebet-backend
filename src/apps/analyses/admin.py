from django.contrib import admin

from .models import (MatchAnalyse, DataSet, DataRow, Analyse)

admin.site.register(DataSet)
admin.site.register(DataRow)
admin.site.register(Analyse)
admin.site.register(MatchAnalyse)
