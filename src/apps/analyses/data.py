from collections import OrderedDict
from csv import DictWriter

import psycopg2
from django.conf import settings
from psycopg2.extras import DictCursor


def _get_team_won(match, matches, team_id):
    return sum(item['datetime'] < match['datetime']
               and item['league_season_id'] == match['league_season_id']
               and item['winner_id'] == team_id for item in matches)

def _get_team_draw(match, matches, team_id):
    return sum(item['datetime'] < match['datetime']
               and item['league_season_id'] == match['league_season_id']
               and (item['home_team_id'] == team_id or item['away_team_id'] == team_id)
               and item['winner_id'] == None for item in matches)


def _get_team_lost(match, matches, team_id):

    return sum(item['datetime'] < match['datetime'] and
               item['league_season_id'] == match['league_season_id']
               and (item['home_team_id'] == team_id or item['away_team_id'] == team_id)
               and item['winner_id'] != None
               and item['winner_id'] != team_id for item in matches)

def _get_team_matches_played(match, matches, team_id):
    return sum(item['datetime'] < match['datetime']
               and item['league_season_id'] == match['league_season_id']
               and (item['home_team_id'] == team_id or item['away_team_id'] == team_id)
               for item in matches)

def _get_team_league_points(match, matches, team_id):
    return 3 * _get_team_won(match, matches, team_id) + _get_team_draw(match, matches, team_id)

def _get_team_home_goals_scored(match, matches, team_id):
    goals = 0
    for item in matches:
        if item['datetime'] < match['datetime'] and item['league_season_id'] == match['league_season_id'] \
            and item['home_team_id'] == team_id:
            goals += item['home_score']
    return goals

def _get_team_home_goals_lost(match, matches, team_id):
    goals = 0
    for item in matches:
        if item['datetime'] < match['datetime'] and item['league_season_id'] == match['league_season_id'] \
            and item['home_team_id'] == team_id:
            goals += item['away_score']
    return goals

def _get_team_away_goals_scored(match, matches, team_id):
    goals = 0
    for item in matches:
        if item['datetime'] < match['datetime'] and item['league_season_id'] == match['league_season_id'] \
            and item['away_team_id'] == team_id:
            goals += item['away_score']
    return goals

def _get_team_away_goals_lost(match, matches, team_id):
    goals = 0
    for item in matches:
        if item['datetime'] < match['datetime'] and item['league_season_id'] == match['league_season_id'] \
            and item['away_team_id'] == team_id:
            goals += item['home_score']
    return goals


def _get_team_home_won(match, matches, team_id):
    return sum(item['datetime'] < match['datetime']
               and item['league_season_id'] == match['league_season_id']
               and item['home_team_id'] == team_id
               and item['winner_id'] == team_id for item in matches)


def _get_team_home_draw(match, matches, team_id):
    return sum(item['datetime'] < match['datetime']
               and item['league_season_id'] == match['league_season_id']
               and item['home_team_id'] == team_id
               and item['winner_id'] == None for item in matches)


def _get_team_home_lost(match, matches, team_id):
    return sum(item['datetime'] < match['datetime'] and
               item['league_season_id'] == match['league_season_id']
               and item['home_team_id'] == team_id
               and item['winner_id'] != None
               and item['winner_id'] != team_id for item in matches)


def _get_team_away_won(match, matches, team_id):
    return sum(item['datetime'] < match['datetime']
               and item['league_season_id'] == match['league_season_id']
               and item['away_team_id'] == team_id
               and item['winner_id'] == team_id for item in matches)


def _get_team_away_draw(match, matches, team_id):
    return sum(item['datetime'] < match['datetime']
               and item['league_season_id'] == match['league_season_id']
               and item['away_team_id'] == team_id
               and item['winner_id'] == None for item in matches)


def _get_team_away_lost(match, matches, team_id):
    return sum(item['datetime'] < match['datetime'] and
               item['league_season_id'] == match['league_season_id']
               and item['away_team_id'] == team_id
               and item['winner_id'] != None
               and item['winner_id'] != team_id for item in matches)


def _get_last_team_statistics(match, matches, team_id):
    last_matches = [item for item in matches if item['datetime'] < match['datetime']\
                    and item['league_season_id'] == match['league_season_id']\
                    and (item['home_team_id'] == team_id or item['away_team_id'] == team_id)]
    last_matches = last_matches[:5]
    won = _get_team_won(match, last_matches, team_id)
    draw = _get_team_draw(match, last_matches, team_id)
    lost = _get_team_lost(match, last_matches, team_id)
    return (won, draw, lost)


def _get_last_direct_matches_statistics(match, matches, home_id, away_id):
    last_matches = [item for item in matches if item['datetime'] < match['datetime']\
                    and item['league_season_id'] == match['league_season_id']\
                    and ((item['home_team_id'] == home_id and item['away_team_id'] == away_id)
                         or item['home_team_id'] == away_id and item['away_team_id'] == home_id)]
    last_matches = last_matches[:5]
    home_won = _get_team_won(match, last_matches, home_id)
    home_draw = _get_team_draw(match, last_matches, home_id)
    home_lost = _get_team_lost(match, last_matches, home_id)
    away_won = _get_team_won(match, last_matches, away_id)
    away_draw = _get_team_draw(match, last_matches, away_id)
    away_lost = _get_team_lost(match, last_matches, away_id)
    return (home_won, home_draw, home_lost, away_won, away_draw, away_lost)


def dump_data_to_csv():
    try:
        db = settings.DATABASES['default']
        conn = psycopg2.connect(
            "dbname='{dbname}' user='{dbuser}' host='{host}' password='{dbpass}'".format(
                dbname=db['NAME'],
                dbuser=db['USER'],
                host=db['HOST'],
                dbpass=db['PASSWORD']
            )
        )
    except:
        print "I am unable to connect to the database"
    else:
        csv_rows = []
        csv_headers = []
        cursor = conn.cursor(cursor_factory=DictCursor)
        cursor.execute("""
          SELECT id, home_score, away_score, datetime,  home_team_id, away_team_id, league_season_id,
          CASE WHEN home_score > away_score THEN home_team_id
               WHEN home_score < away_score THEN away_team_id
          END as winner_id
          FROM statistics_match
          WHERE home_score IS NOT NULL AND away_score IS NOT NULL
          ORDER BY datetime DESC; """)
        rows = cursor.fetchall()

        for row in rows:
            match_data = OrderedDict()
            # match_data['home_team_won'] = _get_team_won(row, rows, row['home_team_id'])
            # match_data['home_team_draw'] = _get_team_draw(row, rows, row['home_team_id'])
            # match_data['home_team_lost'] = _get_team_lost(row, rows, row['home_team_id'])
            match_data['home_team_home_won'] = _get_team_home_won(row, rows, row['home_team_id'])
            match_data['home_team_home_draw'] = _get_team_home_draw(row, rows, row['home_team_id'])
            match_data['home_team_home_lost'] = _get_team_home_lost(row, rows, row['home_team_id'])
            match_data['home_team_away_won'] = _get_team_away_won(row, rows, row['home_team_id'])
            match_data['home_team_away_draw'] = _get_team_away_draw(row, rows, row['home_team_id'])
            match_data['home_team_away_lost'] = _get_team_away_lost(row, rows, row['home_team_id'])
            match_data['home_team_last_won'], match_data['home_team_last_draw'],\
            match_data['home_team_last_lost'] = _get_last_team_statistics(row, rows, row['home_team_id'])
            match_data['home_team_played'] = _get_team_matches_played(row, rows, row['home_team_id'])
            match_data['home_team_points'] = _get_team_league_points(row, rows, row['home_team_id'])
            match_data['home_team_home_scored'] = _get_team_home_goals_scored(row, rows, row['home_team_id'])
            match_data['home_team_home_conceded'] = _get_team_home_goals_lost(row, rows, row['home_team_id'])
            match_data['home_team_away_scored'] = _get_team_away_goals_scored(row, rows, row['home_team_id'])
            match_data['home_team_away_conceded'] = _get_team_away_goals_lost(row, rows, row['home_team_id'])

            # match_data['away_team_won'] = _get_team_won(row, rows, row['away_team_id'])
            # match_data['away_team_draw'] = _get_team_draw(row, rows, row['away_team_id'])
            # match_data['away_team_lost'] = _get_team_lost(row, rows, row['away_team_id'])
            match_data['away_team_home_won'] = _get_team_home_won(row, rows, row['away_team_id'])
            match_data['away_team_home_draw'] = _get_team_home_draw(row, rows, row['away_team_id'])
            match_data['away_team_home_lost'] = _get_team_home_lost(row, rows, row['away_team_id'])
            match_data['away_team_away_won'] = _get_team_away_won(row, rows, row['away_team_id'])
            match_data['away_team_away_draw'] = _get_team_away_draw(row, rows, row['away_team_id'])
            match_data['away_team_away_lost'] = _get_team_away_lost(row, rows, row['away_team_id'])
            match_data['away_team_away_lost'] = _get_team_away_lost(row, rows, row['away_team_id'])
            match_data['away_team_last_won'], match_data['away_team_last_draw'],\
            match_data['away_team_last_lost'] = _get_last_team_statistics(row, rows, row['away_team_id'])
            match_data['away_team_played'] = _get_team_matches_played(row, rows, row['away_team_id'])
            match_data['away_team_points'] = _get_team_league_points(row, rows, row['away_team_id'])
            match_data['away_team_home_scored'] = _get_team_home_goals_scored(row, rows, row['away_team_id'])
            match_data['away_team_home_conceded'] = _get_team_home_goals_lost(row, rows, row['away_team_id'])
            match_data['away_team_away_scored'] = _get_team_away_goals_scored(row, rows, row['away_team_id'])
            match_data['away_team_away_conceded'] = _get_team_away_goals_lost(row, rows, row['away_team_id'])


            match_data['home_team_direct_won'], match_data['home_team_direct_draw'],\
            match_data['home_team_direct_lost'], match_data['away_team_direct_won'],\
            match_data['away_team_direct_draw'], match_data['away_team_direct_lost'] = \
            _get_last_direct_matches_statistics(row, rows, row['home_team_id'], row['away_team_id'])

            if row['winner_id'] == row['home_team_id']:
                match_data['output'] = 1
            elif row['winner_id'] == row['away_team_id']:
                match_data['output'] = 2
            else:
                match_data['output'] = 0

            csv_rows.append(match_data)
    finally:
        conn.close()

    csv_keys = list(csv_rows[0].keys())

    with open(settings.GLOBAL_NEURAL_NETWORK_DATA_FILE_PATH, 'w') as csvfile:
        writer = DictWriter(csvfile, fieldnames=csv_keys)
        writer.writeheader()
        writer.writerows(csv_rows)

def get_upcoming_matches_data():
    try:
        db = settings.DATABASES['default']
        conn = psycopg2.connect(
            "dbname='{dbname}' user='{dbuser}' host='{host}' password='{dbpass}'".format(
                dbname=db['NAME'],
                dbuser=db['USER'],
                host=db['HOST'],
                dbpass=db['PASSWORD']
            )
        )
    except:
        print "I am unable to connect to the database"
    else:
        csv_rows = []
        csv_headers = []
        cursor = conn.cursor(cursor_factory=DictCursor)
        cursor.execute("""
          SELECT id, home_score, away_score, datetime,  home_team_id, away_team_id, league_season_id,
          CASE WHEN home_score > away_score THEN home_team_id
               WHEN home_score < away_score THEN away_team_id
          END as winner_id
          FROM statistics_match
          WHERE home_score IS NOT NULL AND away_score IS NOT NULL
          ORDER BY datetime DESC; """)
        rows = cursor.fetchall()

        upcoming_cursor = conn.cursor(cursor_factory=DictCursor)
        upcoming_cursor.execute("""
          SELECT id, home_score, away_score, datetime,  home_team_id, away_team_id, league_season_id
          FROM statistics_match
          WHERE home_score IS NULL AND away_score IS NULL
          ORDER BY datetime DESC; """)
        upcoming_rows = upcoming_cursor.fetchall()

        for row in upcoming_rows:
            match_data = OrderedDict()
            # match_data['home_team_won'] = _get_team_won(row, rows, row['home_team_id'])
            # match_data['home_team_draw'] = _get_team_draw(row, rows, row['home_team_id'])
            # match_data['home_team_lost'] = _get_team_lost(row, rows, row['home_team_id'])
            match_data['home_team_home_won'] = _get_team_home_won(row, rows, row['home_team_id'])
            match_data['home_team_home_draw'] = _get_team_home_draw(row, rows, row['home_team_id'])
            match_data['home_team_home_lost'] = _get_team_home_lost(row, rows, row['home_team_id'])
            match_data['home_team_away_won'] = _get_team_away_won(row, rows, row['home_team_id'])
            match_data['home_team_away_draw'] = _get_team_away_draw(row, rows, row['home_team_id'])
            match_data['home_team_away_lost'] = _get_team_away_lost(row, rows, row['home_team_id'])
            match_data['home_team_last_won'], match_data['home_team_last_draw'],\
            match_data['home_team_last_lost'] = _get_last_team_statistics(row, rows, row['home_team_id'])
            match_data['home_team_played'] = _get_team_matches_played(row, rows, row['home_team_id'])
            match_data['home_team_points'] = _get_team_league_points(row, rows, row['home_team_id'])
            match_data['home_team_home_scored'] = _get_team_home_goals_scored(row, rows, row['home_team_id'])
            match_data['home_team_home_conceded'] = _get_team_home_goals_lost(row, rows, row['home_team_id'])
            match_data['home_team_away_scored'] = _get_team_away_goals_scored(row, rows, row['home_team_id'])
            match_data['home_team_away_conceded'] = _get_team_away_goals_lost(row, rows, row['home_team_id'])

            # match_data['away_team_won'] = _get_team_won(row, rows, row['away_team_id'])
            # match_data['away_team_draw'] = _get_team_draw(row, rows, row['away_team_id'])
            # match_data['away_team_lost'] = _get_team_lost(row, rows, row['away_team_id'])
            match_data['away_team_home_won'] = _get_team_home_won(row, rows, row['away_team_id'])
            match_data['away_team_home_draw'] = _get_team_home_draw(row, rows, row['away_team_id'])
            match_data['away_team_home_lost'] = _get_team_home_lost(row, rows, row['away_team_id'])
            match_data['away_team_away_won'] = _get_team_away_won(row, rows, row['away_team_id'])
            match_data['away_team_away_draw'] = _get_team_away_draw(row, rows, row['away_team_id'])
            match_data['away_team_away_lost'] = _get_team_away_lost(row, rows, row['away_team_id'])
            match_data['away_team_away_lost'] = _get_team_away_lost(row, rows, row['away_team_id'])
            match_data['away_team_last_won'], match_data['away_team_last_draw'],\
            match_data['away_team_last_lost'] = _get_last_team_statistics(row, rows, row['away_team_id'])
            match_data['away_team_played'] = _get_team_matches_played(row, rows, row['away_team_id'])
            match_data['away_team_points'] = _get_team_league_points(row, rows, row['away_team_id'])
            match_data['away_team_home_scored'] = _get_team_home_goals_scored(row, rows, row['away_team_id'])
            match_data['away_team_home_conceded'] = _get_team_home_goals_lost(row, rows, row['away_team_id'])
            match_data['away_team_away_scored'] = _get_team_away_goals_scored(row, rows, row['away_team_id'])
            match_data['away_team_away_conceded'] = _get_team_away_goals_lost(row, rows, row['away_team_id'])


            match_data['home_team_direct_won'], match_data['home_team_direct_draw'],\
            match_data['home_team_direct_lost'], match_data['away_team_direct_won'],\
            match_data['away_team_direct_draw'], match_data['away_team_direct_lost'] = \
            _get_last_direct_matches_statistics(row, rows, row['home_team_id'], row['away_team_id'])
            match_data.id = row['id']
            csv_rows.append(match_data)
    finally:
        conn.close()
        return csv_rows