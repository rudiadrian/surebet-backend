import csv
import cPickle

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from sklearn.tree import (DecisionTreeClassifier, export_graphviz)
from sklearn.externals.six import StringIO
import pydot
from sklearn.cross_validation import train_test_split
from apps.analyses.data import get_upcoming_matches_data
from apps.analyses.models import DataSet, DataRow, Analyse, MatchAnalyse
from apps.statistics.models import Match


def create_global_decision_tree():
    with open(settings.GLOBAL_NEURAL_NETWORK_DATA_FILE_PATH, 'rb') as csvfile:
        csv_data_set = DataSet.objects.create()
        csv_data_set_rows = []
        dictreader = csv.DictReader(csvfile)
        for row in dictreader:
            csv_data_set_rows.append(DataRow(data_set=csv_data_set, **row))
        DataRow.objects.bulk_create(csv_data_set_rows)

        csvfile.seek(0)

        reader = csv.reader(csvfile, delimiter=',')
        header = reader.next()
        data_set = []
        train_data = []
        train_target = []
        test_data = []
        test_target = []
        test_draw_data = []
        target_draw_data = []
        test_home_data = []
        target_home_data = []
        test_away_data = []
        target_away_data = []

        for i, column in enumerate(header):
            if column == 'output':
                output_column_number = i
        for row in reader:
            for i, col in enumerate(row):
                row[i] = int(col)
            data_set.append(row)
        train_dataset, test_dataset = train_test_split(data_set,
                                                       test_size=0.25,
                                                       random_state=25)
        for row in train_dataset:
            output = row.pop(output_column_number)
            train_data.append(row)
            train_target.append(output)

        for row in test_dataset:
            output = row.pop(output_column_number)
            test_data.append(row)
            test_target.append(output)
            if row[output] == 0:
                test_draw_data.append(row)
                target_draw_data.append(output)
            elif row[output] == 1:
                test_home_data.append(row)
                target_home_data.append(output)
            elif row[output] == 2:
                test_away_data.append(row)
                target_away_data.append(output)

        clf = DecisionTreeClassifier(max_depth=4)
        clf = clf.fit(train_data, train_target)
        mean_accuracy =  clf.score(test_data, test_target)
        home_accuracy = clf.score(test_home_data, target_home_data)
        draw_accuracy = clf.score(test_draw_data, target_draw_data)
        away_accuracy = clf.score(test_away_data, target_away_data)

        dot_data = StringIO()
        export_graphviz(clf, out_file=dot_data)
        graph = pydot.graph_from_dot_data(dot_data.getvalue())
        graph.write_pdf("iris.pdf")

        import pdb; pdb.set_trace()

        try:
            old_analyse = Analyse.objects.get(slug='decision_tree')
        except ObjectDoesNotExist:
            pass
        else:
            old_analyse.delete()

        analyse = Analyse.objects.create(data_set=csv_data_set,
                                         name="Decision tree",
                                         slug="decision_tree",
                                         mean_accuracy=mean_accuracy,
                                         home_accuracy=home_accuracy,
                                         draw_accuracy=draw_accuracy,
                                         away_accuracy=away_accuracy)

        with open(settings.GLOBAL_DECISION_TREE_FILE_PATH, 'wb') as file:
            cPickle.dump(clf, file)


def analyze_global_upcoming_matches():
    with open(settings.GLOBAL_DECISION_TREE_FILE_PATH) as file:
        clf = cPickle.load(file)
    reader = get_upcoming_matches_data()
    analysis = Analyse.objects.get(slug='decision_tree')
    for row in reader:
        outcome = clf.predict_proba([row.values()])[0]
        print outcome
        match = Match.objects.get(id=row.id)
        MatchAnalyse.objects.create(analyse=analysis,
                                    match=match, draw_outcome=outcome[0],
                                    home_win_outcome=outcome[1],
                                    away_win_outcome=outcome[2])
