# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0004_auto_20150117_1343'),
    ]

    operations = [
        migrations.CreateModel(
            name='Analyse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=35)),
                ('slug', models.SlugField(unique=True, max_length=35)),
                ('mean_accuracy', models.DecimalField(max_digits=9, decimal_places=8)),
                ('draw_accuracy', models.DecimalField(max_digits=9, decimal_places=8)),
                ('home_accuracy', models.DecimalField(max_digits=9, decimal_places=8)),
                ('away_accuracy', models.DecimalField(max_digits=9, decimal_places=8)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('home_team_home_won', models.PositiveIntegerField()),
                ('home_team_home_draw', models.PositiveIntegerField()),
                ('home_team_home_lost', models.PositiveIntegerField()),
                ('home_team_away_won', models.PositiveIntegerField()),
                ('home_team_away_draw', models.PositiveIntegerField()),
                ('home_team_away_lost', models.PositiveIntegerField()),
                ('home_team_last_won', models.PositiveIntegerField()),
                ('home_team_last_draw', models.PositiveIntegerField()),
                ('home_team_last_lost', models.PositiveIntegerField()),
                ('home_team_played', models.PositiveIntegerField()),
                ('home_team_points', models.PositiveIntegerField()),
                ('home_team_home_scored', models.PositiveIntegerField()),
                ('home_team_home_conceded', models.PositiveIntegerField()),
                ('home_team_away_scored', models.PositiveIntegerField()),
                ('home_team_away_conceded', models.PositiveIntegerField()),
                ('away_team_home_won', models.PositiveIntegerField()),
                ('away_team_home_draw', models.PositiveIntegerField()),
                ('away_team_home_lost', models.PositiveIntegerField()),
                ('away_team_away_won', models.PositiveIntegerField()),
                ('away_team_away_draw', models.PositiveIntegerField()),
                ('away_team_away_lost', models.PositiveIntegerField()),
                ('away_team_last_won', models.PositiveIntegerField()),
                ('away_team_last_draw', models.PositiveIntegerField()),
                ('away_team_last_lost', models.PositiveIntegerField()),
                ('away_team_played', models.PositiveIntegerField()),
                ('away_team_points', models.PositiveIntegerField()),
                ('away_team_home_scored', models.PositiveIntegerField()),
                ('away_team_home_conceded', models.PositiveIntegerField()),
                ('away_team_away_scored', models.PositiveIntegerField()),
                ('away_team_away_conceded', models.PositiveIntegerField()),
                ('home_team_direct_won', models.PositiveIntegerField()),
                ('home_team_direct_draw', models.PositiveIntegerField()),
                ('home_team_direct_lost', models.PositiveIntegerField()),
                ('away_team_direct_won', models.PositiveIntegerField()),
                ('away_team_direct_draw', models.PositiveIntegerField()),
                ('away_team_direct_lost', models.PositiveIntegerField()),
                ('output', models.PositiveIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MatchAnalyse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('home_win_outcome', models.DecimalField(max_digits=9, decimal_places=8)),
                ('draw_outcome', models.DecimalField(max_digits=9, decimal_places=8)),
                ('away_win_outcome', models.DecimalField(max_digits=9, decimal_places=8)),
                ('analyse', models.ForeignKey(related_name='match_analyses', to='analyses.Analyse')),
                ('match', models.OneToOneField(to='statistics.Match')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='datarow',
            name='data_set',
            field=models.ForeignKey(related_name='rows', to='analyses.DataSet'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analyse',
            name='data_set',
            field=models.OneToOneField(related_name='analysis', to='analyses.DataSet'),
            preserve_default=True,
        ),
    ]
