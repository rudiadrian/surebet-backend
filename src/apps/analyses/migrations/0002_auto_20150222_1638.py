# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analyses', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matchanalyse',
            name='match',
            field=models.ForeignKey(related_name='analyses', to='statistics.Match'),
            preserve_default=True,
        ),
    ]
