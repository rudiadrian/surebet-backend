import csv
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from pybrain.supervised.trainers.backprop import BackpropTrainer

from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure.modules import (SoftmaxLayer, SigmoidLayer, TanhLayer)
from pybrain.datasets import ClassificationDataSet
from pybrain.supervised.trainers import RPropMinusTrainer
from pybrain.tools.xml.networkreader import NetworkReader
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.utilities import percentError
from apps.analyses.data import get_upcoming_matches_data
from apps.analyses.models import MatchAnalyse, DataSet, DataRow, Analyse
from apps.statistics.models import Match


def create_global_neural_network():
    net = buildNetwork(36, 9, 3, hiddenclass=TanhLayer, outclass=SigmoidLayer)
    dataset = ClassificationDataSet(36, 1, nb_classes=3)

    with open(settings.GLOBAL_NEURAL_NETWORK_DATA_FILE_PATH, 'rb') as csvfile:
        csv_data_set = DataSet.objects.create()
        csv_data_set_rows = []
        dictreader = csv.DictReader(csvfile)
        for row in dictreader:
            csv_data_set_rows.append(DataRow(data_set=csv_data_set, **row))
        DataRow.objects.bulk_create(csv_data_set_rows)

        csvfile.seek(0)
        reader = csv.reader(csvfile, delimiter=',')

        header = reader.next()
        for i, column in enumerate(header):
            if column == 'output':
                output_column_number = i
        for row in reader:
            for i, col in enumerate(row):
                row[i] = int(col)
            output = row.pop(output_column_number)
            dataset.addSample(row, [output])

        tstdata, trndata = dataset.splitWithProportion( 0.25 )

        home_dataset = ClassificationDataSet(36, 1, nb_classes=3)
        draw_dataset = ClassificationDataSet(36, 1, nb_classes=3)
        away_dataset = ClassificationDataSet(36, 1, nb_classes=3)

        for input, target in tstdata:
            if target == 1:
                home_dataset.addSample(input, [target])
            elif target == 0:
                draw_dataset.addSample(input, [target])
            elif target == 2:
                away_dataset.addSample(input, [target])

        home_dataset._convertToOneOfMany()
        draw_dataset._convertToOneOfMany()
        away_dataset._convertToOneOfMany()

        trndata._convertToOneOfMany()
        tstdata._convertToOneOfMany()

        trainer = BackpropTrainer(net, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01)

        for i in xrange(20):
            trainer.trainEpochs(5)

        trnresult = percentError( trainer.testOnClassData(),
                                  trndata['class'] )
        tstresult = percentError( trainer.testOnClassData(
               dataset=tstdata ), tstdata['class'] )

        tst_mean_accuracy = (100 - tstresult) / 100.0
        tst_home_accuracy = (100 - percentError(trainer.testOnClassData(home_dataset),
                                               home_dataset['class'])) / 100.0
        tst_draw_accuracy = (100 - percentError(trainer.testOnClassData(draw_dataset),
                                               draw_dataset['class'])) / 100.0
        tst_away_accuracy = (100 - percentError(trainer.testOnClassData(away_dataset),
                                               away_dataset['class'])) / 100.0

        print "epoch: %4d" % trainer.totalepochs, \
              "  train error: %5.2f%%" % trnresult, \
              "  test error: %5.2f%%" % tstresult

        try:
            old_analyse = Analyse.objects.get(slug='neural_network')
        except ObjectDoesNotExist:
            pass
        else:
            old_analyse.delete()

        analyse = Analyse.objects.create(data_set=csv_data_set,
                                         name="Neural network",
                                         slug="neural_network",
                                         mean_accuracy=tst_mean_accuracy,
                                         home_accuracy=tst_home_accuracy,
                                         draw_accuracy=tst_draw_accuracy,
                                         away_accuracy=tst_away_accuracy)

        NetworkWriter.writeToFile(net, settings.GLOBAL_NEURAL_NETWORK_FILE_PATH)


def analyze_global_upcoming_matches():
    net = NetworkReader.readFrom(settings.GLOBAL_NEURAL_NETWORK_FILE_PATH)
    reader = get_upcoming_matches_data()
    analysis = Analyse.objects.get(slug='neural_network')
    for row in reader:
        outcome = net.activate(row.values())
        print outcome
        match = Match.objects.get(id=row.id)
        MatchAnalyse.objects.create(analyse=analysis,
                                    match=match, draw_outcome=outcome[0],
                                    home_win_outcome=outcome[1],
                                    away_win_outcome=outcome[2])