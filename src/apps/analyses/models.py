from django.db import models

from apps.statistics.models import (Match)


class DataSet(models.Model):
    pass


class DataRow(models.Model):
    data_set = models.ForeignKey(DataSet, related_name='rows')
    home_team_home_won = models.PositiveIntegerField()
    home_team_home_draw = models.PositiveIntegerField()
    home_team_home_lost = models.PositiveIntegerField()
    home_team_away_won = models.PositiveIntegerField()
    home_team_away_draw = models.PositiveIntegerField()
    home_team_away_lost = models.PositiveIntegerField()
    home_team_last_won = models.PositiveIntegerField()
    home_team_last_draw = models.PositiveIntegerField()
    home_team_last_lost = models.PositiveIntegerField()
    home_team_played = models.PositiveIntegerField()
    home_team_points = models.PositiveIntegerField()
    home_team_home_scored = models.PositiveIntegerField()
    home_team_home_conceded = models.PositiveIntegerField()
    home_team_away_scored = models.PositiveIntegerField()
    home_team_away_conceded = models.PositiveIntegerField()
    away_team_home_won = models.PositiveIntegerField()
    away_team_home_draw = models.PositiveIntegerField()
    away_team_home_lost = models.PositiveIntegerField()
    away_team_away_won = models.PositiveIntegerField()
    away_team_away_draw = models.PositiveIntegerField()
    away_team_away_lost = models.PositiveIntegerField()
    away_team_last_won = models.PositiveIntegerField()
    away_team_last_draw = models.PositiveIntegerField()
    away_team_last_lost = models.PositiveIntegerField()
    away_team_played = models.PositiveIntegerField()
    away_team_points = models.PositiveIntegerField()
    away_team_home_scored = models.PositiveIntegerField()
    away_team_home_conceded = models.PositiveIntegerField()
    away_team_away_scored = models.PositiveIntegerField()
    away_team_away_conceded = models.PositiveIntegerField()
    home_team_direct_won = models.PositiveIntegerField()
    home_team_direct_draw = models.PositiveIntegerField()
    home_team_direct_lost = models.PositiveIntegerField()
    away_team_direct_won = models.PositiveIntegerField()
    away_team_direct_draw = models.PositiveIntegerField()
    away_team_direct_lost = models.PositiveIntegerField()
    output = models.PositiveIntegerField()


class Analyse(models.Model):
    data_set = models.OneToOneField(DataSet, related_name='analysis')
    name = models.CharField(max_length=35)
    slug = models.SlugField(max_length=35, unique=True)
    mean_accuracy = models.DecimalField(max_digits=9, decimal_places=8)
    draw_accuracy = models.DecimalField(max_digits=9, decimal_places=8)
    home_accuracy = models.DecimalField(max_digits=9, decimal_places=8)
    away_accuracy = models.DecimalField(max_digits=9, decimal_places=8)

    def __unicode__(self):
        return self.name

class MatchAnalyse(models.Model):
    match = models.ForeignKey(Match, related_name='analyses')
    analyse = models.ForeignKey(Analyse, related_name='match_analyses')
    home_win_outcome = models.DecimalField(max_digits=9, decimal_places=8)
    draw_outcome = models.DecimalField(max_digits=9, decimal_places=8)
    away_win_outcome = models.DecimalField(max_digits=9, decimal_places=8)

    def __unicode__(self):
        return u'{match}'.format(match=self.match)

