from django.core.management.base import BaseCommand
from apps.analyses.models import MatchAnalyse

import apps.analyses.neural_networks as nn
import apps.analyses.decision_trees as dt


class Command(BaseCommand):
    def handle(self, *args, **options):
        MatchAnalyse.objects.all().delete()
        nn.analyze_global_upcoming_matches()
        dt.analyze_global_upcoming_matches()