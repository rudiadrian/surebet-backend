from django.core.management.base import BaseCommand

from ...decision_trees import create_global_decision_tree


class Command(BaseCommand):
    def handle(self, *args, **options):
        create_global_decision_tree()