from django.core.management.base import BaseCommand

from ...neural_networks import create_global_neural_network

class Command(BaseCommand):
    def handle(self, *args, **options):
        create_global_neural_network()
