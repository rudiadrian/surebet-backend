from django.core.management.base import BaseCommand

from ...data import dump_data_to_csv

class Command(BaseCommand):

    def handle(self, *args, **options):
        dump_data_to_csv()