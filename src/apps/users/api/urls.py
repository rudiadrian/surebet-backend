from django.conf.urls import patterns, url

from . import views


patterns = patterns(
    url(r'^$', views.User.as_view(), name='users-user')
)