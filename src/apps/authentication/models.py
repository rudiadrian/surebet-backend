from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from apps.analyses.models import Analyse


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    classifier = models.ForeignKey(Analyse, null=True, blank=True)

    def __unicode__(self):
        return self.user.username


def create_profile(sender, instance, created, **kwargs):
    if created:
        profile = UserProfile(user=instance)
        profile.save()

post_save.connect(create_profile, sender=User)