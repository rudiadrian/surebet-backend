from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = format_suffix_patterns([
    url(r'^user/$', views.User.as_view(), name='auth-user'),
    url(r'^login/$', views.Login.as_view(), name='auth-login'),
    url(r'^logout/$', views.Logout.as_view(), name='auth-logout'),
    url(r'^register/$', views.Register.as_view(), name='auth-register')
])