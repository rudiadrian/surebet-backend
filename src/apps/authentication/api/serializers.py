from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError
from apps.authentication.models import UserProfile



class NewUserSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise ValidationError("E-Mail address is already registered.")
        return value

    def validate_password(self, value):
        secure_password = make_password(value)
        return secure_password

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'email', 'password')


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('classifier',)


class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(read_only=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    userprofile = UserProfileSerializer()

    def update(self, instance, validated_attrs):
        profile_data = validated_attrs.pop('userprofile')

        instance.first_name = validated_attrs.get('first_name', instance.first_name)
        instance.last_name = validated_attrs.get('last_name', instance.last_name)
        instance.email = validated_attrs.get('email', instance.email)
        instance.save()

        instance.userprofile.classifier = profile_data.get('classifier', instance.userprofile.classifier)
        instance.userprofile.save()

        return instance

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'userprofile')


class TokenSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Token
        fields = ('key', 'user')