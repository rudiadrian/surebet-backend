from django.contrib.auth import login, logout
from rest_framework import generics
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.views import APIView
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.authtoken.models import Token
from rest_framework import status

from . import serializers


class Login(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = AuthTokenSerializer
    token_model = Token
    response_serializer = serializers.TokenSerializer

    def login(self):
        self.user = self.serializer.validated_data['user']
        self.token, created = self.token_model.objects.get_or_create(user=self.user)
        login(self.request, self.user)

    def get_response(self):
        return Response(self.response_serializer(self.token).data, status=status.HTTP_200_OK)

    def get_error_response(self):
        return Response(self.serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        self.serializer = self.get_serializer(data=self.request.DATA)
        if not self.serializer.is_valid():
            return self.get_error_response()
        self.login()
        return self.get_response()


class Logout(APIView):
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request):
        try:
            request.user.auth_token.delete()
        except:
            pass
        logout(request)

        return Response({"success": "Successfully logged out."}, status=status.HTTP_200_OK)


class User(RetrieveUpdateAPIView):
    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        return self.request.user


class Register(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.NewUserSerializer